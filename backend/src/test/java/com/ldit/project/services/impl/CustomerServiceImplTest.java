package com.ldit.project.services.impl;

import com.ldit.project.models.PersonName;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

//@RunWith(MockitoJUnitRunner.class)
@RunWith(SpringRunner.class)
@Log4j2
public class CustomerServiceImplTest {
    @InjectMocks
    CustomerServiceImpl service;

    @Before
    public void setUp() throws Exception {
        //MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createCustomer() {

    }

    @Test
    public void testSplitNamesWithTitleWithMiddleWithSuffix(){
        PersonName test = service.splitNames("Mr. Randy L. Tolentino Jr.");
        assertThat(test.getTitle(), is("Mr."));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is("L."));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is("Jr."));
    }

    @Test
    public void testSplitNamesWithTitleWithMiddleWithoutSuffix(){
        PersonName test = service.splitNames("Mr. Randy L. Tolentino");
        assertThat(test.getTitle(), is("Mr."));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is("L."));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is(""));
    }

    @Test
    public void testSplitNamesWithoutTitleWithMiddleWithSuffix(){
        PersonName test = service.splitNames("Randy L. Tolentino Jr.");
        assertThat(test.getTitle(), is(""));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is("L."));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is("Jr."));
    }

    @Test
    public void testSplitNamesWithTitleWithoutMiddleWithSuffix(){
        PersonName test = service.splitNames("Mr. Randy Tolentino Jr.");
        assertThat(test.getTitle(), is("Mr."));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is(""));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is("Jr."));
    }

    @Test
    public void testSplitNamesWithTitleWithoutMiddleWithoutSuffix(){
        PersonName test = service.splitNames("Mr. Randy Tolentino");
        assertThat(test.getTitle(), is("Mr."));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is(""));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is(""));
    }

    @Test
    public void testSplitNamesWithoutTitleWithoutMiddleWithSuffix(){
        PersonName test = service.splitNames("Randy Tolentino Sr.");
        assertThat(test.getTitle(), is(""));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is(""));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is("Sr."));
    }

    @Test
    public void testSplitNamesWithWhiteSpaces() {
        PersonName test = service.splitNames("  Mr.   Randy   L.   Tolentino Sr ");
        assertThat(test.getTitle(), is("Mr."));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is("L."));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is("Sr"));
    }

    @Test
    public void testSplitNamesWithoutTitleWithMiddleWithoutSuffix() {
        PersonName test = service.splitNames("Randy L. Tolentino");
        assertThat(test.getTitle(), is(""));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is("L."));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is(""));
    }

    @Test
    public void testSplitNamesWithoutTitleWithoutMiddleWithoutSuffix() {
        PersonName test = service.splitNames("Randy Tolentino");
        assertThat(test.getTitle(), is(""));
        assertThat(test.getFirstName(), is("Randy"));
        assertThat(test.getMiddleName(), is(""));
        assertThat(test.getLastName(), is("Tolentino"));
        assertThat(test.getSuffix(), is(""));
    }

    @Test
    public void testNameWithTitleWithSuffixInLoweCase() {
        PersonName test = service.splitNames("mr. randy jr.");
        assertThat(test.getTitle(), is("mr."));
        assertThat(test.getFirstName(), is("randy"));
        assertThat(test.getMiddleName(), is(""));
        assertThat(test.getLastName(), is(""));
        assertThat(test.getSuffix(), is("jr."));
    }
}