package com.ldit.project.services;

import com.ldit.project.models.Address;
import com.ldit.project.models.Customer;
import com.ldit.project.models.PersonName;
import com.ldit.project.models.response.customer.GetCustomersResponse;
import com.ldit.project.repositories.CustomerRepository;
import com.ldit.project.services.impl.CustomerServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.manipulation.Sortable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class /*SpringRunner.class*/)
@Log4j2
public class CustomerServiceTest {
    @InjectMocks
    CustomerServiceImpl service;

    @Mock
    CustomerRepository customerRepo;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        List<Customer> customers = new ArrayList<>();
        Customer customer = new Customer();
        PersonName personName = new PersonName();
        personName.setFirstName("Randy");
        customer.setPersonName(personName);

        List<Address> addresses = new ArrayList<>();
        Address address = new Address();
        address.setCustomer(customer);
        address.setPostal("0910");

        addresses.add(address);
        customer.setAddresses(addresses);
        customers.add(customer);

        Customer customer2 = new Customer();
        PersonName personName2 = new PersonName();
        personName2.setFirstName("2Randy");
        customer2.setPersonName(personName2);

        List<Address> addresses2 = new ArrayList<>();
        Address address2 = new Address();
        address2.setCustomer(customer);
        address2.setPostal("0910");

        addresses2.add(address2);
        customer2.setAddresses(addresses2);
        customers.add(customer2);

        Page<Customer> customers1= new PageImpl<>(customers);

        //Mockito.when(customerRepo.findAll(Mockito.any(Pageable.class))).thenReturn(customers1);
    }



    @Test
    @Profile({"test"})
    public void testGetBiDirectionalReletaionship(){
        /*List<GetCustomersResponse> customers = service.getAllCustomers(1, 1);
        if(customers!=null && customers.size()>0){
            log.info("postal from Customer class: " + customers.get(0).getDetails().getAddresses().get(0).getPostal());
            assertThat(customers.get(0).getDetails().getAddresses().size(), is(greaterThan(0)));

            log.info("postal from outside Customer class: " + customers.get(0).getAddresses().get(0).getPostal());
            assertThat(customers.get(0).getAddresses().size(), is(greaterThan(0)));
        }*/
    }
}