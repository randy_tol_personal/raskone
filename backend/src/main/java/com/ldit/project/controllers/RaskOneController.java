package com.ldit.project.controllers;

import com.ldit.project.models.RaskoneSampleModel;
import com.ldit.project.repositories.RaskOneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "api/v1")
@RestController
public class RaskOneController {
    @Autowired
    RaskOneRepository raskOneRepo;

    @GetMapping(value = "/getHeroes")
    public List<RaskoneSampleModel> getHeroes(){
        return raskOneRepo.findAll();
    }

    @GetMapping(value = "/searchHeroes/{name}")
    public List<RaskoneSampleModel> searchHeroes(@PathVariable String name){
        return raskOneRepo.findAllByNameContaining(name);
    }

    @GetMapping(value = "/getHero/{id}")
    public RaskoneSampleModel getHero(@PathVariable long id){
        return raskOneRepo.findById(id).orElse(new RaskoneSampleModel());
    }

    @DeleteMapping(value = "/deleteHero/{id}")
    public void deleteHero(@PathVariable long id){
        RaskoneSampleModel rask1 = this.getHero(id);
        if(rask1.getId()!=null)
            raskOneRepo.delete(rask1);
    }

    @PutMapping(value = "/updateHero")
    public RaskoneSampleModel updateHero(@RequestBody RaskoneSampleModel rask1){
        return raskOneRepo.save(rask1);
    }

    @PostMapping(value = "/createHero")
    public RaskoneSampleModel createHero(@RequestBody RaskoneSampleModel rask1){
        return raskOneRepo.save(rask1);
    }
}
