package com.ldit.project.controllers;

import com.ldit.project.models.Address;
import com.ldit.project.models.Customer;
import com.ldit.project.models.response.customer.CustomersListWithPage;
import com.ldit.project.models.response.customer.GetCustomersResponse;
import com.ldit.project.repositories.AddressRepository;
import com.ldit.project.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping(value = "api/v1")
@RestController
public class CustomerServiceController {
    @Autowired
    CustomerService customerService;

    @Autowired
    AddressRepository addressRepo;

    @GetMapping(value = "/getCustomerAddressById/{customerId}/{addressIndex}")
    public List<Address> getCustomerAddressById(@PathVariable long customerId, int addressIndex){
        return customerService.getCustomerAndAddressById(customerId, addressIndex);
    }

    @GetMapping(value = "/getAddress/{id}")
    public Address getAddressById(@PathVariable long id){
        return addressRepo.findById(id).orElse(new Address());
    }

    @GetMapping(value = {"/getCustomers", "/getCustomers/{count}/{page}/{customerId}"})
    public CustomersListWithPage getCustomers(@PathVariable Optional<Integer> count, @PathVariable Optional<Integer> page, @PathVariable Optional<Long> customerId){
        if(count.isPresent())
            return customerService.getAllCustomers(count.get(), page.get(), customerId.get());

        return customerService.getAllCustomers(0, 0, 0);
    }

    /*@GetMapping(value = "/searchCustomers/{name}")
    public List<Customer> searchCustomers(@PathVariable String name){
        return customerRepo.findAllByNameContaining(name);
    }*/

    @GetMapping(value = "/getCustomer/{id}")
    public GetCustomersResponse getCustomer(@PathVariable long id){
        return customerService.getCustomerById(id);
    }

    @DeleteMapping(value = "/deleteCustomer/{id}")
    public Customer deleteCustomer(@PathVariable long id){
        return customerService.deleteCustomer(id);
    }

    @PutMapping(value = "/updateCustomer")
    public GetCustomersResponse updateCustomer(@RequestBody GetCustomersResponse customer){
        return customerService.updateCustomer(customer);
    }

    @PostMapping(value = "/createCustomer")
    public GetCustomersResponse createCustomer(@RequestBody GetCustomersResponse customer){
        return customerService.createCustomer(customer);
    }
}
