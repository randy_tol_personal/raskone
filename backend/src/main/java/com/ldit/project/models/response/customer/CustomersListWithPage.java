package com.ldit.project.models.response.customer;

import lombok.Data;

import java.util.List;

@Data
public class CustomersListWithPage {
    long total;
    long page;
    List<GetCustomersResponse> customerList;

    public CustomersListWithPage(long t, long p, List<GetCustomersResponse> c){
        this.total = t;
        this.page = p;
        this.customerList = c;
    }
}
