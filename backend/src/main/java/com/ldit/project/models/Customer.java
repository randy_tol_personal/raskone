package com.ldit.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.ldit.project.models.common.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
public class Customer extends BaseEntity<Long> implements Serializable {

    @Embedded
    private PersonName personName;

    @Embedded
    private ContactDetails contactDetails;

    @JsonProperty("yId")
    private String yId;
    private String customerNo;

    @OneToMany(cascade={CascadeType.ALL}, fetch = FetchType.EAGER,
            mappedBy = "customer")
    //@JoinColumn(name = "address_id")
    @JsonIgnore
    private List<Address> addresses;

    private String fullName;

    /*public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = (personName.getTitle()+ " " +personName.getFirstName()+ " " + personName.getMiddleName()+ " " +personName.getLastName()+ " " +personName.getSuffix()).trim();
    }*/

    /*@JsonInclude
    public String getFullName(){
        if(personName!=null)
            return ;
        else
            return this.fullName;
    }*/
}
