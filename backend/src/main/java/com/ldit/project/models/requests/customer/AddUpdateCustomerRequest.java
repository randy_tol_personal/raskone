package com.ldit.project.models.requests.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AddUpdateCustomerRequest {
    private String customerNo;
    private String telNo;
    private String address;
    private String postal;
    private String customerName;
    private String emailAddress;
    @JsonProperty("yId")
    private String yId;
    private String billingAddress;
    private String billingPostal;
}
