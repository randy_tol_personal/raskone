package com.ldit.project.models;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class PersonName implements Serializable {

    private String title, firstName, middleName, lastName, suffix;

}
