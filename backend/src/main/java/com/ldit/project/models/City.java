package com.ldit.project.models;

import com.ldit.project.models.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class City extends BaseEntity<Long> implements Serializable {

    private String name;
    private String name2;

}
