package com.ldit.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ldit.project.models.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "address")
public class Address extends BaseEntity<Long> implements Serializable {

    private String line1, line2, postal;

    @ManyToOne(targetEntity = City.class)
    private City city;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    private boolean isBilling = false;


}
