package com.ldit.project.models;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class ContactDetails implements Serializable {

    private String phoneNumber, email;
}
