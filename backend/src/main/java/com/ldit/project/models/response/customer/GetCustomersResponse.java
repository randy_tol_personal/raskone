package com.ldit.project.models.response.customer;

import com.ldit.project.models.Address;
import com.ldit.project.models.Customer;
import lombok.Data;

import java.util.List;

@Data
public class GetCustomersResponse {
    Customer details;
    List<Address> addresses;
}
