package com.ldit.project.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="raskone")
public class RaskoneSampleModel {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
}
