package com.ldit.project.services;

import com.ldit.project.models.Address;
import com.ldit.project.models.Customer;
import com.ldit.project.models.response.customer.CustomersListWithPage;
import com.ldit.project.models.response.customer.GetCustomersResponse;

import java.util.List;

public interface CustomerService {
    GetCustomersResponse createCustomer(GetCustomersResponse inputCustomer);
    GetCustomersResponse updateCustomer(GetCustomersResponse inputCustomer);
    Customer deleteCustomer(long customerId);
    CustomersListWithPage getAllCustomers(int count, int page, long customerId);
    GetCustomersResponse getCustomerById(long customerId);
    List<Address> getCustomerAndAddressById(long customerId, int idIndex);
}
