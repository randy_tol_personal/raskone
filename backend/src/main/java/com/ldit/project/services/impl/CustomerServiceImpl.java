package com.ldit.project.services.impl;

import com.ldit.project.models.Address;
import com.ldit.project.models.City;
import com.ldit.project.models.Customer;
import com.ldit.project.models.PersonName;
import com.ldit.project.models.response.customer.CustomersListWithPage;
import com.ldit.project.models.response.customer.GetCustomersResponse;
import com.ldit.project.repositories.AddressRepository;
import com.ldit.project.repositories.CityRepository;
import com.ldit.project.repositories.CustomerRepository;
import com.ldit.project.services.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Log4j2
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerRepository customerRepo;

    @Autowired
    AddressRepository addressRepo;

    @Autowired
    CityRepository cityRepo;

    public PersonName splitNames(String name){
        PersonName personName = new PersonName();
        //let's divide cutomerName to first, middle and last

        String[] customerName = name.trim().split("\\s+", 5);
        String title = "", firstName = "", middleName = "", lastName = "", suffix = "";
        boolean hastTitle = false, hasSuffix = false;

        if (name.trim().toLowerCase().startsWith("mrs") || name.trim().toLowerCase().startsWith("mr") || name.trim().toLowerCase().startsWith("ms")) {
            title = customerName[0];
            if(customerName.length>1)
                firstName = customerName[1];
            hastTitle = true;
        }
        else
            firstName = customerName[0];

        if (name.trim().toLowerCase().endsWith("jr") || name.trim().toLowerCase().endsWith("jr.") ||
                name.trim().toLowerCase().endsWith("sr") || name.trim().toLowerCase().endsWith("sr.")) {
            suffix = customerName[customerName.length-1];
            if((hastTitle && customerName.length>3) || (!hastTitle && customerName.length>2))
                lastName = customerName[customerName.length-2];
            hasSuffix = true;
        }
        else
            lastName = customerName[customerName.length-1];

        switch (customerName.length){
            case 5:
                if(hastTitle && hasSuffix)
                    middleName = customerName[2];
                else if(hastTitle && !hasSuffix){
                    middleName = customerName[2];
                    lastName = customerName[3] + " " + customerName[4];
                }
                else if(!hastTitle && hasSuffix){
                    middleName = customerName[2];
                    firstName = customerName[0] + " " + customerName[1];
                }
                else if(!hastTitle && !hasSuffix){
                    middleName = customerName[2];
                    firstName = customerName[0] + " " + customerName[1];
                    lastName = customerName[3] + " " + customerName[4];
                }
                break;
            case 4: //Mr. Randy L. Tolentino
                if(hastTitle && !hasSuffix)
                    middleName = customerName[2];
                //Randy L. Tolentino Jr.
                else if(!hastTitle && hasSuffix)
                    middleName = customerName[1];

                //Randy L. Tolentino Something
                else if(!hastTitle && !hasSuffix){
                    middleName = customerName[1];
                    lastName = customerName[2] + " " + customerName[3];
                }
                break;

            case 3: //Randy L. Tolentino
                if(!hastTitle && !hasSuffix)
                    middleName = customerName[1];
                break;
        }

        personName.setTitle(title);
        personName.setFirstName(firstName);
        personName.setMiddleName(middleName);
        personName.setLastName(lastName);
        personName.setSuffix(suffix);
        return personName;
    }

    @Override
    public GetCustomersResponse createCustomer(GetCustomersResponse inputCustomer) {
        Customer customer =  inputCustomer.getDetails();
        inputCustomer.getDetails().setPersonName(this.splitNames(customer.getFullName()));

        customerRepo.save(customer);

        List<Address> addresses = new ArrayList<>();
        Address address = inputCustomer.getAddresses().get(0);
        //TODO set city by postal
        City city = cityRepo.save(new City());

        address.setCity(city);
        address.setCustomer(customer);

        addressRepo.save(address);
        addresses.add(address);

        Address billingAddress = inputCustomer.getAddresses().get(1);

        //TODO set city by billing postal
        City billingCity = cityRepo.save(new City());

        billingAddress.setCity(billingCity);
        billingAddress.setCustomer(customer);

        addressRepo.save(billingAddress);
        addresses.add(billingAddress);

        customer.setAddresses(addresses);
        customerRepo.save(customer);
        return inputCustomer;
    }

    @Override
    public GetCustomersResponse updateCustomer(GetCustomersResponse inputCustomer) {
        GetCustomersResponse customerResponse = new GetCustomersResponse();
        Customer customer = customerRepo.findById(inputCustomer.getDetails().getId()).orElse(new Customer());
        if(customer.getId() != null){
            customer = inputCustomer.getDetails();
            customer.setPersonName(this.splitNames(inputCustomer.getDetails().getFullName()));
            customer.setAddresses(inputCustomer.getAddresses());
            customer.setContactDetails(inputCustomer.getDetails().getContactDetails());
            customerRepo.save(customer);
        }
        return customerResponse;
    }

    @Override
    public Customer deleteCustomer(long customerId) {
        Customer customer = customerRepo.findById(customerId).orElse(new Customer());
        if(customer.getId() != null)
            customerRepo.delete(customer);
        return customer;
    }

    @Override
    public CustomersListWithPage getAllCustomers(int count, int page, long customerId) {
        List<Customer> customers;
        int i;
        long total;

        if(count > 0){
            //if for count and page == 10 of 1
            i=page;
            Pageable sortedById = PageRequest.of(page-1, count, Sort.by("id").ascending());
            Page<Customer> cust = customerRepo.findAll(sortedById);
            total = cust.getTotalElements();
            customers = (List) cust.getContent();
        }
        else {
            //else for count, page is regardless, customerId ==== 1, 0, 99

            Iterable<Customer> cust = customerRepo.findAll(Sort.by("id"));
            customers = (List) cust;
            total = customers.size();

            if (customerId > 0) {
                //GetCustomersResponse getCustomersResponse = this.getCustomerById(customerId);
                //final Customer findingCustomer = getCustomersResponse.getDetails();

                //override index and customers
                Customer findingCustomer = customers.stream().filter(customer -> customer.getId().equals(customerId)).findAny().orElse(null);
                i = customers.indexOf(findingCustomer) + 1;
                /*int j=i=1;
                for (Customer customer: customers){
                    if(findingCustomer.getId().equals(customer.getId())) {
                        i=j;
                        break;
                    }
                    j++;
                }*/

                List<Customer> newCust = new ArrayList<>();
                newCust.add(findingCustomer);
                customers = newCust;
            }

            else
                i=1;
        }

        List<GetCustomersResponse> getCustomersResponseList = new ArrayList<>();
        for(Customer customer : customers){
            GetCustomersResponse customerResponse = new GetCustomersResponse();
            customerResponse.setDetails(customer);
            customerResponse.setAddresses(customer.getAddresses());
            getCustomersResponseList.add(customerResponse);
        }
        return new CustomersListWithPage(total, i, getCustomersResponseList);
    }

    @Override
    public GetCustomersResponse getCustomerById(long customerId) {
        GetCustomersResponse getCustomersResponse =  new GetCustomersResponse();
        Customer customer = customerRepo.findById(customerId).orElse(new Customer());
        if(customer.getId() != null){
            getCustomersResponse.setDetails(customer);
            getCustomersResponse.setAddresses(customer.getAddresses());
        }
        return getCustomersResponse;
    }

    @Override
    public List<Address> getCustomerAndAddressById(long customerId, int idIndex) {
        Customer customer = customerRepo.findById(customerId).orElse(new Customer());

        if(customer.getId() != null) {
            log.info("postal: " + customer.getAddresses().get(idIndex).getPostal());
        }

        return customer.getAddresses();
    }


}
