package com.ldit.project.repositories;

import com.ldit.project.models.RaskoneSampleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RaskOneRepository extends JpaRepository<RaskoneSampleModel, Long> {
    @Query("Select r from RaskoneSampleModel r where r.name like %:name%")
    List<RaskoneSampleModel> findAllByNameContaining(String name);
}
