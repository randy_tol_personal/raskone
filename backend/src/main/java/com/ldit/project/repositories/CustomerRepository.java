package com.ldit.project.repositories;

import com.ldit.project.models.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long>{
    @Query("Select r from Customer r where r.personName.firstName like %:name% " +
            "or r.personName.middleName like %:name% " +
            "or r.personName.lastName like %:name%")
    List<Customer> findAllByNameContaining(String name);
    List<Customer> findAllByOrderByIdAsc();
    //List<Customer> findAllById(Long price, Pageable pageable);
}
