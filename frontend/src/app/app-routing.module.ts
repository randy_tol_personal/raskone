import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddNewComponent } from './add-new/add-new.component';

const routes: Routes = [{ path: 'home', component: HomeComponent},
                        { path: 'home/:page', component: HomeComponent},
                        { path: '', redirectTo: '/home', pathMatch: 'full' },
                        { path: 'customerView/:mode/:id', component: AddNewComponent},
                        { path: 'customerView/:mode', component: AddNewComponent}];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}
