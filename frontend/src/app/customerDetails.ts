import { Customer } from './customer';
import { Address } from './address';

export class CustomerDetails {
  details: Customer;
  addresses : Address[];
  checked: boolean;
  hidden: boolean;
  tempIndexId: number;
}
