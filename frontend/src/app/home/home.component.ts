import {Component, OnInit} from '@angular/core';
import {CustomerDetails} from '../customerDetails';
import {Customer} from '../customer';
import {ContactDetails} from '../contactDetails';
import {Address} from '../address';
import {CustomerService} from '../customer.service';

import {ExportExcelService} from '../export-excel.service';
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  customers: CustomerDetails[] = [];
  itemsPerPage: number;
  currentPage: number;
  page: number;
  previousPage: number;
  selectAll: boolean;

  filteredItems: CustomerDetails[] = [];

  hasAllLoaded: boolean;
  total: number;

  constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private excelJsService: ExportExcelService,
    ) {
    this.titleService.setTitle('Asiakkaat');
  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        const loadThisPage = params['page'];
        console.log(params)

        this.itemsPerPage = 10;
        this.hasAllLoaded = false;
        this.currentPage=loadThisPage;

        this.initLoad(loadThisPage ? loadThisPage : 1);
      });
  }

  prevNextPage(custs: {total:number, page:number, customerList:CustomerDetails[]}) : void{
    this.currentPage = custs.page;
    this.filteredItems = custs.customerList.map( cust => {
      cust.addresses[0].address = cust.addresses[0].line1;
      cust.addresses[0].address += cust.addresses[0].line2!=null? ' ' + cust.addresses[0].line2 : '';
      cust.addresses[0].postal = cust.addresses[0].postal;
      cust.addresses[0].postal += cust.addresses[0].city.name!=null? ' ' + cust.addresses[0].city.name : '';

      cust.addresses[1].address = cust.addresses[1].line1;
      cust.addresses[1].address += cust.addresses[1].line2!=null? ' ' + cust.addresses[1].line2 : '';
      cust.addresses[1].postal = cust.addresses[1].postal;
      cust.addresses[1].postal += cust.addresses[1].city.name!=null? ' ' + cust.addresses[1].city.name : '';
      return cust;
    });
  }

  initLoad(whatPage: number) : void{
    this.getCustomers(whatPage, true, this.itemsPerPage);
    this.selectAll = false;
  }

  getCustomers(whatPage:number, repeatOnce: boolean, initialSize? : number): void {
    this.customerService.getCustomers(initialSize, (initialSize? whatPage : null), (initialSize? 0 : null))
      .subscribe(custs => {
        console.log(custs);
        //this.currentPage = custs.page; dont know why and when
        //whatPage = custs.page; dont know why and when
        this.customers = custs.customerList;
        this.total = custs.total;
        this.assignCopy();
        this.loadData(repeatOnce? 1 : whatPage);

        if(repeatOnce)
          this.getCustomers(whatPage, false);
        else
          this.hasAllLoaded=true;
      });
  }

  exportSelected() : void{
    var toExport = this.filteredItems.filter(customer => customer.checked === true).map( x =>
      JSON.parse(JSON.stringify(
          [x.details.id, x.details.customerNo, x.details.contactDetails.phoneNumber,
                x.addresses[0].address,x.addresses[0].postal,
                x.details.fullName, x.details.contactDetails.email, x.details.yId, x.addresses[1].address, x.addresses[1].postal])
                  .replace(new RegExp("<label class='req'>",'gi'), "" )
                  .replace(new RegExp("</label>",'gi'), "" )
        )
    );
    this.excelJsService.exportToExcel(['Asiakas_ID','Asiakasnro','Puhelinnumero','Osoite','Postinumero Ja Toimipaikka','Asiakkaan Nimi','Sahkoposti Osoite','Y Tunnus','Laskutusosoite','Laskutus  Postinumero Ja Toimipaikka'], toExport)
  }

  selectUnselectAll(): void {
    this.selectAll = !this.selectAll;

    for(let cust of this.pageSlice(this.filteredItems)){
      cust.checked=this.selectAll;
    }
  }

  onSelectItemsPerPageChange(): void{
    if(!this.hasAllLoaded){
      console.log(this.itemsPerPage);
      this.currentPage=1;
      this.initLoad(this.currentPage);
    }
  }

  pageSlice(custs: CustomerDetails[]) : CustomerDetails[]{
    return custs.slice((this.page-1) * this.itemsPerPage , (this.page-1) * this.itemsPerPage + this.itemsPerPage)
  }

  loadPage(page: number) : void {
    if (page !== this.previousPage) {
      this.previousPage = page;
    }
  }

  delete(event, customer: CustomerDetails): void {
    console.log(customer);
    this.customerService.deleteCustomer(customer)
      .subscribe(customer => {
        this.loadData(1) });
  }

  loadData(whatPage: number) {
    this.page=whatPage;
    this.previousPage=whatPage-1;
    this.loadPage(whatPage);
  }

  assignCopy(){
    this.filteredItems = Object.assign([], this.mapVisibleCustomerDetails());
  }

  filterItem(event){
    var value = event.searchVal;
    var mapContentsArr = [];

    if(!value){
      this.assignCopy();
    }
    else{
      this.filteredItems = Object.assign([], this.mapVisibleCustomerDetails())
        .filter(item => {
          var toPush = this.mapContentsOnly(item);

          var isFiltered = JSON.stringify(toPush).toLowerCase().indexOf(value.toLowerCase()) > -1
          if(isFiltered)
            mapContentsArr.push(toPush);

          return isFiltered;
        })
      .map ((item, index) => {
          const toReplace = new RegExp(value,'gi');
          const replaceWith = "<label class='req'>" + value + '</label>';

          mapContentsArr[index] = JSON.parse (JSON.stringify(mapContentsArr[index]).replace(toReplace, replaceWith ));

          item.details.id = mapContentsArr[index][0] + '';
          item.details.fullName = mapContentsArr[index][1];
          item.details.customerNo = mapContentsArr[index][2];
          item.details.yId = mapContentsArr[index][3];
          item.details.contactDetails.phoneNumber = mapContentsArr[index][4];
          item.details.contactDetails.email = mapContentsArr[index][5];
          item.addresses[0].address = mapContentsArr[index][6];
          item.addresses[0].postal = mapContentsArr[index][7];
          item.addresses[1].address = mapContentsArr[index][8];
          item.addresses[1].postal = mapContentsArr[index][9];

          return item;
        });
    }
  }

  mapContentsOnly(item: CustomerDetails): [string, any, string, string, string, string, string, string, string, string]{
    return [item.details.id + '', item.details.fullName, item.details.customerNo, item.details.yId
    , item.details.contactDetails.phoneNumber, item.details.contactDetails.email
    , item.addresses[0].address, item.addresses[0].postal
    , item.addresses[1].address, item.addresses[1].postal];
  }

  mapVisibleCustomerDetails(): CustomerDetails[]{
    return this.customers.map( item => {
        var custDetail = new CustomerDetails();

        var cust = new Customer();
        cust.id = item.details.id +'';
        cust.fullName = item.details.fullName;
        cust.customerNo = item.details.customerNo;
        cust.yId = item.details.yId;

        var custContact = new ContactDetails();
        custContact.phoneNumber = item.details.contactDetails.phoneNumber;
        custContact.email = item.details.contactDetails.email;
        cust.contactDetails = custContact;

        custDetail.details = cust;

        var address = new Address();
        address.billing = item.addresses[0].billing;
        //address.line1 = item.addresses[0].line1;
        address.address = item.addresses[0].line1;
        address.address += item.addresses[0].line2!=null? ' ' + item.addresses[0].line2 : '';
        address.postal = item.addresses[0].postal;
        address.postal += item.addresses[0].city.name!=null? ' ' + item.addresses[0].city.name : '';

        var billing = new Address();
        billing.billing = item.addresses[1].billing;
        //billing.line1 = item.addresses[1].line1;
        billing.address = item.addresses[1].line1;
        billing.address += item.addresses[1].line2!=null? ' ' + item.addresses[1].line2 : '';
        billing.postal = item.addresses[1].postal;
        billing.postal += item.addresses[1].city.name!=null? ' ' + item.addresses[1].city.name : '';

        custDetail.addresses = [address, billing];

        return custDetail;
    })
  }
}
