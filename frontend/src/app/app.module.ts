import {NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CustomerSearchComponent} from './customer-search/customer-search.component';
import {MessagesComponent} from './messages/messages.component';
import {HomeComponent} from './home/home.component';
import {RaskOneMenuComponent} from './rask-one-menu/rask-one-menu.component';
import {AddNewComponent} from './add-new/add-new.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LineInsertionComponent} from './line-insertion/line-insertion.component';
import {ExportExcelService} from './export-excel.service';
import {PrevNextModule} from "./prev-next/prev-next.module";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    PrevNextModule
  ],
  declarations: [
    AppComponent,
    MessagesComponent,
    CustomerSearchComponent,
    HomeComponent,
    RaskOneMenuComponent,
    AddNewComponent,
    LineInsertionComponent
  ],
  providers: [ExportExcelService, Title],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
