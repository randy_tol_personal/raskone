import { ContactDetails } from './contactDetails';

export class Customer {
  id: string;
  customerNo : string;
  address : string;
  postal : string;
  yId : string;
  creationDate: string;
  lastModificationDate: string;
  contactDetails: ContactDetails;
  fullName: string;
}
