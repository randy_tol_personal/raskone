import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Customer } from './customer';
import { CustomerDetails } from './customerDetails';
import { MessageService } from './message.service';

const httpOptions = {
headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class CustomerService {

  //private customersUrl = 'api/customers';  // URL to web api

  private API_SERVER = "http://127.0.0.1:8080/api/v1";

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getCustomers (count? : number, page? : number, id? : number): Observable<{total: number, page: number, customerList: CustomerDetails[]}> {
    return this.http.get<{total: number, page: number, customerList: CustomerDetails[]}>(`${this.API_SERVER}/getCustomers` + (count ? (id>0 ? `/0` : `/${count}`) : ``) + (page ? (id>0 ?  `/0` : `/${page}`) : ``) + (id!=null ? `/${id}` : ``))
      .pipe(
        tap(_ => this.log('fetched customers')),
        catchError(this.handleError('getCustomers', {total:0, page:0, customerList:[]}))
      );
  }

  /** GET customer by id. Return `undefined` when id not found */
  getCustomerNo404<Data>(id: number): Observable<CustomerDetails> {
    const url = `${this.API_SERVER}/getCustomer/${id}`;
    return this.http.get<CustomerDetails[]>(url)
      .pipe(
        map(customers => customers[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} customer id=${id}`);
        }),
        catchError(this.handleError<CustomerDetails>(`getCustomer id=${id}`))
      );
  }

  /** GET customer by id. Will 404 if id not found */
  getCustomer(id: number): Observable<CustomerDetails> {
    const url = `${this.API_SERVER}/getCustomer/${id}`;
    return this.http.get<CustomerDetails>(url).pipe(
      tap(_ => this.log(`fetched customer id=${id}`)),
      catchError(this.handleError<CustomerDetails>(`getCustomer id=${id}`))
    );
  }

  /* GET customers whose name contains search term */
  searchCustomers(term: string): Observable<CustomerDetails[]> {
    if (!term.trim()) {
      // if not search term, return empty customer array.
      return of([]);
    }
    return this.http.get<CustomerDetails[]>(`${this.API_SERVER}/searchCustomers/${term}`).pipe(
      tap(_ => this.log(`found customers matching "${term}"`)),
      catchError(this.handleError<CustomerDetails[]>('searchCustomers', []))
    );
  }

  //////// Save methods //////////

  /** POST: add a new customer to the server */
  addCustomer (customer: CustomerDetails): Observable<CustomerDetails> {
    return this.http.post<CustomerDetails>(`${this.API_SERVER}/createCustomer`, customer, httpOptions).pipe(
      tap((newCustomer: CustomerDetails) => this.log(`added customer w/ id=${newCustomer.details.id}`)),
      catchError(this.handleError<CustomerDetails>('addCustomer'))
    );
  }

  /** DELETE: delete the customer from the server */
  deleteCustomer (customer: CustomerDetails | number): Observable<CustomerDetails> {
    console.log(customer);
    const id = typeof customer === 'number' ? customer : customer.details.id;
    const url = `${this.API_SERVER}/deleteCustomer/${id}`;

    return this.http.delete<CustomerDetails>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted customer id=${id}`)),
      catchError(this.handleError<CustomerDetails>('deleteCustomer'))
    );
  }

  /** PUT: update the customer on the server */
  updateCustomer (customer: CustomerDetails): Observable<any> {
    return this.http.put(`${this.API_SERVER}/updateCustomer`, customer, httpOptions).pipe(
      tap(_ => this.log(`updated customer id=${customer.details.id}`)),
      catchError(this.handleError<any>('updateCustomer'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a CustomerService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`CustomerService: ${message}`);
  }
}
