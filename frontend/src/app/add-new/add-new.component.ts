import {Component, Input, OnInit} from '@angular/core';

import {Customer} from '../customer';
import {ContactDetails} from '../contactDetails';
import {Address} from '../address';
import {CustomerDetails} from '../customerDetails';
import {CustomerService} from '../customer.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.less']
})
export class AddNewComponent implements OnInit {
  @Input()
  customerDetails: CustomerDetails;


  subTitle: string;
  mode: string;
  input: string;
  customerId: number;
  currentPage: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private customerService: CustomerService,
    private location: Location,
    private titleService: Title
    ) { }

  prevNextPage(idOrPage: {id: number, page: number}) {
    if(idOrPage.id)
      this.router.navigate(['/customerView/' + this.mode + '/' + idOrPage.id] );
    else if(idOrPage.page)
      this.currentPage=idOrPage.page;
    else
      this.currentPage=1;
  }

  ngOnInit() {
    this.route.params.subscribe(
        params => {
            const mode = params['mode'];
            this.mode = mode;
            if(mode == "edit" || mode == "view"){
              this.customerId =  +params['id'];
              this.subTitle = "Customer, "+ ((mode == "edit") ? "Edit" : ( mode == "view" ? "View" : "")) +" [" + this.customerId + "]";
              this.input= (mode == "edit") ? "input" : "label";
              this.customerService.getCustomer(this.customerId)
                .subscribe(customerDetails => this.customerDetails = customerDetails)
            }
            else if(mode == "add"){
              this.customerDetails = new CustomerDetails();
              this.customerDetails.details = new Customer();
              this.customerDetails.details.contactDetails = new ContactDetails();
              var billing = new Address();
              billing.line1 = 'Sama kuin osoite';
              billing.postal = 'Sama kuin postinumero ja toimipaikka';
              this.customerDetails.addresses = [new Address(), billing];
              this.input="input";
              this.subTitle = "Customers, Add New";
            }
          this.titleService.setTitle(this.subTitle);
        }
    );
  }

  add(customerDetails: CustomerDetails): void {
    customerDetails.addresses[0].billing=false;
    customerDetails.addresses[1].billing=true;

    this.customerService.addCustomer(customerDetails)
          .subscribe(customer => {
            this.goBack();
          });
  }

  edit(customerDetails : CustomerDetails): void {
    this.customerService.updateCustomer(customerDetails)
          .subscribe(customerDetails => {
            this.goBack();
          });
  }

  delete(customer: CustomerDetails): void {
    this.customerService.deleteCustomer(customer)
      .subscribe(customer => {
        //this.gotoPage('/home' +this.currentPage? '/' + Math.ceil(this.currentPage / 10) : '' ) }
        this.gotoPage('/home' + (this.currentPage? '/' + this.currentPage : '') ) }
        );
  }


  goBack(): void{
    this.location.back();
  }

  gotoPage(pageName: String): void{
    this.router.navigate([`${pageName}`]);
  }
}
