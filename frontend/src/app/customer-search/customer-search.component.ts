import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CustomerDetails} from '../customerDetails';
import {CustomerService} from '../customer.service';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
selector: 'app-customer-search',
templateUrl: './customer-search.component.html',
styleUrls: [ './customer-search.component.less' ]
})
export class CustomerSearchComponent implements OnInit {
customers$: Observable<CustomerDetails[]>;
private searchTerms = new Subject<string>();

constructor(private customerService: CustomerService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.customers$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.customerService.searchCustomers(term)),
    );
  }

  @Output() searchEvent = new EventEmitter();
  filterItem($event, searchVal: string): void {
    this.searchEvent.emit({event, searchVal})
  }


}
