import { TestBed } from '@angular/core/testing';

import { PrevNextService } from './prev-next.service';

describe('PrevNextService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrevNextService = TestBed.get(PrevNextService);
    expect(service).toBeTruthy();
  });
});
