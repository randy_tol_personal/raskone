import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PrevNextService} from "./prev-next.service";
import {CustomerService} from "../customer.service";
import {CustomerDetails} from "../customerDetails";

@Component({
  selector: 'app-prev-next',
  templateUrl: './prev-next.component.html',
  styleUrls: ['./prev-next.component.less']
})
export class PrevNextComponent implements OnInit {

  @Input()
  public currentPage: number;

  private total: number;

  @Input('paginationMode')
  public paginationMode: string;

  @Input('pageSize')
  public pageSize: number;

  @Input('customerId')
  public customerId: number;

  @Output()
  private customerDetails = new EventEmitter<{total:number, page:number, customerList: CustomerDetails[]}>();

  @Output()
  private prevNextId = new EventEmitter<{id:number, page:number}>();

  constructor(
    private prevNextService: PrevNextService,
    private customerService: CustomerService) { }

  ngOnInit() {
    this.currentPage = 1;
    //this.text = this.prevNextService.getText();
    this.loadPage(this.pageSize, this.currentPage, this.customerId, false)
  }

  private loadPage(count: number, whatPage : number, id: number, emitNextPage: boolean) : void{
    this.customerService.getCustomers(count, whatPage, id ).subscribe(
      customerLoad => {
        this.currentPage = customerLoad.page;
        this.total = customerLoad.total;

        if(emitNextPage){
          if(this.paginationMode == 'editView') {
            this.prevNextId.emit({id: +customerLoad.customerList[0].details.id, page: null});
          }
          else if(this.paginationMode == 'home') {
            this.customerDetails.emit(customerLoad);
          }
        }
        else{
          if(this.paginationMode == 'editView') {
            this.prevNextId.emit({id: null, page: Math.ceil(this.currentPage / 10) });
          }
        }
      }
    );
  }

  private prevNextClick(prevNext: number) : void{
      this.loadPage(this.pageSize, this.currentPage += prevNext, 0, true);
  }
}
