import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PrevNextComponent} from "./prev-next.component";
import {PrevNextService} from "./prev-next.service";
import {RoundPipe} from "../round.pipe";

@NgModule({
  declarations: [PrevNextComponent, RoundPipe],
  exports: [PrevNextComponent],
  imports: [CommonModule],
  bootstrap: [PrevNextComponent],
  providers: [ PrevNextService ]
})
export class PrevNextModule { }
