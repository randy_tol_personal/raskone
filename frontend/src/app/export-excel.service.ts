import * as Excel from "exceljs/dist/exceljs.min.js";
import * as ExcelProper from "exceljs";
import * as FileSaver from 'file-saver';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExportExcelService {

  constructor() {
  }

  blobType: string;
  name: string;
  sName: string;
  excelFileName: string;
  data: any;
  cols: any

  exportToExcel(exportCols, exportData): void{
    this.blobType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

    /*this.cols=['Column1','Column2','Column3','Column4','Column5']
    this.data=[{col1:"a1",col2:"b1",col3:"c1",col4:"d1",col5:"e1"},
        {col1:"a2",col2:"b2",col3:"c2",col4:"d2",col5:"e2"},
        {col1:"a3",col2:"b3",col3:"c3",col4:"d3",col5:"e3"},
        {col1:"a4",col2:"b4",col3:"c4",col4:"d4",col5:"e4"},
        {col1:"a5",col2:"b5",col3:"c5",col4:"d5",col5:"e5"}]*/

    this.sName = "Export";
    this.excelFileName = "asiakkaat";

    var workbook = new Excel.Workbook();
    workbook.creator = 'Ranran';
    workbook.lastModifiedBy = 'Ranran';
    workbook.created = new Date();
    workbook.modified = new Date();
    /*workbook.addWorksheet(this.sName, { views: [{ activeCell: 'A1', showGridLines: false }] })
    var sheet = workbook.getWorksheet(1);
    var head1 = ["Exported Data"];
    sheet.addRow("Any Information");
    sheet.addRow("Any Information");
    sheet.addRow("Any Information");
    sheet.getRow("");
    sheet.getRow(5).values = "Ranran";  // Adding columns as a header
    sheet.columns = [
      { key: 'Col1' },
      { key: 'Col2' },
      { key: 'Col3' },
      { key: 'Col4' },
      { key: 'Col5' }
    ];*/

    workbook.addWorksheet(this.sName, { views: [{ /*state: 'frozen', ySplit: 3, xSplit: 2, */ activeCell: 'A1', showGridLines: false }] })
    var sheet = workbook.getWorksheet(1);

    /*var head1 = ["Exported Data"];
    sheet.addRow(head1);
    sheet.addRow("");*/

    sheet.getRow(1).values = exportCols;
    sheet.columns = [
      { key: 'col1' },
      { key: 'col2' },
      { key: 'col3' },
      { key: 'col4' },
      { key: 'col5' },
      { key: 'col6' },
      { key: 'col7' },
      { key: 'col8' },
      { key: 'col9' },
      { key: 'col10' }
    ];

    sheet.addRows(exportData);
    workbook.xlsx.writeBuffer().then(data => {
      var blob = new Blob([data], { type: this.blobType });
      var url = window.URL.createObjectURL(blob);

     /* var a = document.createElement("a");
      a.download = this.excelFileName;
      a.click();*/

     FileSaver.saveAs(blob, this.excelFileName);
    });
  }
}
