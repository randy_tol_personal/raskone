import { City } from './city';

export class Address {
  id: number;
  billing: boolean;
  address : string;
  line1 : string;
  line2 : string;
  postal : string;
  city : City;
}
