import { AfterViewInit, Component, ElementRef, ViewChild, OnInit, Input, Output, EventEmitter, ViewContainerRef, ComponentFactoryResolver} from '@angular/core';
import { CustomerDetails } from '../customerDetails';
import { Customer } from '../customer';
import { ContactDetails } from '../contactDetails';
import { Address } from '../address';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-line-insertion',
  templateUrl: './line-insertion.component.html',
  styleUrls: ['./line-insertion.component.less', '../home/home.component.less']
})

export class LineInsertionComponent implements OnInit, AfterViewInit {
  @Input()
  customerDetails: CustomerDetails[];

  elementIndex : number;

  constructor(
    private customerService: CustomerService,
    private elRef:ElementRef
  ) {}

  ngOnInit() {
    this.elementIndex = 0;
    //this.initializeCopy();
    //this.function2();
  }

  ngAfterViewInit() {
  }

  @Output() menuEvent = new EventEmitter();
  function2(){
    //calling menu here
    this.menuEvent.emit(null)
  }

  doCopy(){
    console.log("linInsertEvent from Menu called doCopy()")
    this.initializeCopy();
    this.elementIndex++;
  }

  initializeCopy() : void{
    //initialize and push to array
    if(this.elementIndex == 0)
      this.customerDetails = [new CustomerDetails()];
    else
      this.customerDetails[this.elementIndex] = new CustomerDetails();

    this.customerDetails[this.elementIndex].hidden = false;
    this.customerDetails[this.elementIndex].tempIndexId = this.elementIndex;
    this.customerDetails[this.elementIndex].details = new Customer();
    this.customerDetails[this.elementIndex].details.contactDetails = new ContactDetails();
    var billing = new Address();
    billing.line1 = 'Sama kuin osoite';
    billing.postal = 'Sama kuin postinumero ja toimipaikka';
    this.customerDetails[this.elementIndex].addresses = [new Address(), billing];
  }

  @Output() minusMinusEvent = new EventEmitter();
  cancel(event, i: number): void {
    if(event)
      console.log("cancelling...." + i);

    this.customerDetails[i].hidden = true;
    this.elRef.nativeElement.querySelector('#table' + i).style.visibility='hidden';
    this.elRef.nativeElement.querySelector('#table' + i).style.display='none';

    this.minusMinusEvent.emit(null);
  }

  cancelAll() : void {
    var newCustomersUnhidden = this.customerDetails.filter(
          customer => customer.hidden === false);

    //similar to forEach
    for(const {item, index} of newCustomersUnhidden.map( (item, index) => ({item, index})) ){
      console.log(index + ' ' + item.tempIndexId);
      this.cancel(null, item.tempIndexId)
    }
  }

  saveAll() : void {
    this.addAllCustomers(this.customerDetails.filter(
          customer => customer.hidden === false));
  }

  @Output() initLoadEvent = new EventEmitter();
  emitInitLoad(): void{
    this.initLoadEvent.emit(null);
  }

  addAllCustomers(custs : CustomerDetails[]): void{
      custs[0].addresses[0].billing=false;
      custs[0].addresses[1].billing=true;

      this.customerService.addCustomer(custs[0])
        .subscribe(customer => {
          //do the recursion after hiding
          this.cancel(null, custs[0].tempIndexId);
          var recurCust = this.customerDetails.filter(customer => customer.hidden === false);

          /*console.log("recurring")
          console.log(recurCust);*/

          if(recurCust.length>0)
            this.addAllCustomers(recurCust);
          else
            this.emitInitLoad(); //end recursion with loading
        });
  }

  addCustomer(customerDetails: CustomerDetails, i: number): void {
    customerDetails.addresses[0].billing=false;
    customerDetails.addresses[1].billing=true;

    this.customerService.addCustomer(customerDetails)
      .subscribe(customer => {
        this.cancel(null, i);
        this.emitInitLoad();
      });
  }
}
