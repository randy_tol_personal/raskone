import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineInsertionComponent } from './line-insertion.component';

describe('LineInsertionComponent', () => {
  let component: LineInsertionComponent;
  let fixture: ComponentFixture<LineInsertionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineInsertionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineInsertionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
