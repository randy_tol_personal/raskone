export class ContactDetails {
  email: string;
  phoneNumber: string;
}
