import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaskOneMenuComponent } from './rask-one-menu.component';

describe('RaskOneMenuComponent', () => {
  let component: RaskOneMenuComponent;
  let fixture: ComponentFixture<RaskOneMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaskOneMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaskOneMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
