import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
//import { LineInsertionComponent } from '../line-insertion/line-insertion.component';

@Component({
  selector: 'app-rask-one-menu',
  templateUrl: './rask-one-menu.component.html',
  styleUrls: ['./rask-one-menu.component.less','../../assets/bootstrap/bootstrap.min.css']
})

export class RaskOneMenuComponent implements OnInit {
  constructor() {}
  ngOnInit() {
    this.selectAll=false;
    this.lineInserted = 0;
  }

  selectAll: boolean;

  //@Input
  lineInserted: number; //use this variable to temporarily hide/unhide buttons of saveAll/cancelAll

  minusMinus(): void{
    this.lineInserted--;
  }

  @Output() exportSelectedEvent = new EventEmitter();
  exportSelected(event): void {
    //calling Export Selected  here
    console.log("from menu: calling Export Selected");
    this.exportSelectedEvent.emit(null)
  }

  @Output() lineInsertEvent = new EventEmitter();
  lineInsert(event): void {
    //calling linInsertion here
    console.log("from menu: calling Line Insertion");
    this.lineInserted++;

    this.lineInsertEvent.emit(null)
  }

  @Output() selectUnselectEvent = new EventEmitter();
  selectUnselectAll(event): void {
    this.selectAll = !this.selectAll;

    console.log("from menu: calling Select Unselect");

    this.selectUnselectEvent.emit(null)
  }

  @Output() cancelAllEvent = new EventEmitter();
  cancelAll(event): void {
    console.log("from menu: calling Cancel All")

    this.cancelAllEvent.emit(null)
  }

  @Output() saveAllEvent = new EventEmitter();
  saveAll(event): void {
    console.log("from menu: calling Save All")
    this.saveAllEvent.emit(null)
  }

  function1(): void{
    console.log("menu function called.");
  }

}

